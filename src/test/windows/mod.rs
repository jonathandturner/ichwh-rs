use {super::common_macros::cleanup_test_dir, crate::*, std::env::set_var};

#[async_std::test]
async fn file_not_exec() {
    let dir = build_test_folder!("file-not-exec").await;

    create_file!(dir, "test.txt").await;

    let path_with_ext = which_in_dir("test.txt", &dir).await.unwrap();
    let path_no_ext = which_in_dir("test", &dir).await.unwrap();

    assert_eq!(path_with_ext, None);
    assert_eq!(path_no_ext, None);

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn find_com_file() {
    let dir = dbg!(build_test_folder!("find-com-file").await);

    create_file!(dir, "test.COM").await;

    let expected_path = Some(path!(dir.to_str().unwrap(), "test.COM"));
    let actual_path_with_ext = which_in_dir("test.com", &dir).await.unwrap();
    let actual_path_no_ext = which_in_dir("test", &dir).await.unwrap();

    assert_eq!(&actual_path_with_ext, &expected_path);
    assert_eq!(&actual_path_no_ext, &expected_path);

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn find_exe_file() {
    let dir = dbg!(build_test_folder!("find-exe-file").await);

    create_file!(dir, "test.EXE").await;

    let expected_path = Some(path!(dir.to_str().unwrap(), "test.EXE"));
    let actual_path_with_ext = which_in_dir("test.exe", &dir).await.unwrap();
    let actual_path_no_ext = which_in_dir("test", &dir).await.unwrap();

    assert_eq!(&actual_path_with_ext, &expected_path);
    assert_eq!(&actual_path_no_ext, &expected_path);

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn find_bat_file() {
    let dir = dbg!(build_test_folder!("find-bat-file").await);

    create_file!(dir, "test.BAT").await;

    let expected_path = Some(path!(dir.to_str().unwrap(), "test.BAT"));
    let actual_path_with_ext = which_in_dir("test.bat", &dir).await.unwrap();
    let actual_path_no_ext = which_in_dir("test", &dir).await.unwrap();

    assert_eq!(&actual_path_with_ext, &expected_path);
    assert_eq!(&actual_path_no_ext, &expected_path);

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn find_cmd_file() {
    let dir = dbg!(build_test_folder!("find-cmd-file").await);

    create_file!(dir, "test.CMD").await;

    let expected_path = Some(path!(dir.to_str().unwrap(), "test.CMD"));
    let actual_path_with_ext = which_in_dir("test.cmd", &dir).await.unwrap();
    let actual_path_no_ext = which_in_dir("test", &dir).await.unwrap();

    assert_eq!(&actual_path_with_ext, &expected_path);
    assert_eq!(&actual_path_no_ext, &expected_path);

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn respect_pathext_order() {
    let dir = dbg!(build_test_folder!("respect-pathext-order").await);

    create_file!(dir, "test.EXE").await;
    create_file!(dir, "test.BAT").await;

    let expected_path = Some(path!(dir.to_str().unwrap(), "test.EXE"));
    let actual_path = which_in_dir("test", &dir).await.unwrap();

    assert_eq!(&actual_path, &expected_path);

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn which_all_respects_order() {
    let dir = build_test_folder!("which-all-respects-order").await;

    create_file!(dir, "test.EXE").await;
    create_file!(dir, "test.BAT").await;
    create_file!(dir, "test.cmd").await;
    create_file!(dir, "test.com").await;

    let expected_order = vec![
        path!(dir.to_str().unwrap(), "test.com"),
        path!(dir.to_str().unwrap(), "test.EXE"),
        path!(dir.to_str().unwrap(), "test.BAT"),
        path!(dir.to_str().unwrap(), "test.cmd"),
    ];

    let actual_order = which_all_in_dir("test", &dir).await.unwrap();

    assert_eq!(&actual_order, &expected_order);

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn case_insensitive() {
    let dir = build_test_folder!("case-insensitive").await;

    create_file!(dir, "TeSt.cOm").await;
    let expected_path = Some(path!(dir.to_str().unwrap(), "TeSt.cOm"));

    assert_eq!(which_in_dir("test", &dir).await.unwrap(), expected_path);
    assert_eq!(which_in_dir("TEST", &dir).await.unwrap(), expected_path);
    assert_eq!(which_in_dir("TeSt", &dir).await.unwrap(), expected_path);
    assert_eq!(which_in_dir("test.com", &dir).await.unwrap(), expected_path);
    assert_eq!(which_in_dir("test.COM", &dir).await.unwrap(), expected_path);
    assert_eq!(which_in_dir("TEST.COM", &dir).await.unwrap(), expected_path);
    assert_eq!(which_in_dir("TEST.com", &dir).await.unwrap(), expected_path);

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn local_file() {
    let dir = build_test_folder!("local-file").await;

    let cwd = std::env::current_dir().unwrap();
    std::env::set_current_dir(&dir).unwrap();

    create_file!(dir, "test.CMD").await;
    let expected_path = Some(
        path!(dir.to_str().unwrap(), "test.CMD")
            .canonicalize()
            .await
            .unwrap(),
    );

    assert_eq!(which(".\\test").await.unwrap(), expected_path);
    assert_eq!(which(".\\test.cmd").await.unwrap(), expected_path);
    assert_eq!(which("..\\local-file\\test").await.unwrap(), expected_path);

    std::env::set_current_dir(&cwd).unwrap();
    cleanup_test_dir(&dir).await;
}

/// End-to-end test
#[async_std::test]
async fn path_search() {
    let dir = build_test_folder!("path-search").await;

    let a = build_subdir!(&dir, "a").await;
    create_file!(a, "foo.exe").await;
    create_file!(a, "bar.exe").await;

    let b = build_subdir!(&dir, "b").await;
    create_file!(b, "bar.exe").await;

    // Ensure it still works with a nonexistent folder on the path
    let nonexistent = path!(dir.to_str().unwrap(), "not_real");

    // Set the PATH variable to this test
    let path_var = format!(
        "{};{};{}",
        nonexistent.to_str().unwrap(),
        a.to_str().unwrap(),
        b.to_str().unwrap(),
    );
    dbg!(&path_var);
    set_var("PATH", &path_var);

    // `.../test_root/path-searches/a/foo`
    let expected_path = Some(path!(a.to_str().unwrap(), "foo.exe"));
    let path = which("foo").await.unwrap();
    assert_eq!(path, expected_path);

    // `.../test_root/path-searches/b/baz`
    let expected_path = Some(path!(a.to_str().unwrap(), "bar.exe"));
    let path = which("bar").await.unwrap();
    assert_eq!(path, expected_path);

    // Test `which_all`
    let expected_paths = vec![
        path!(a.to_str().unwrap(), "bar.exe"),
        path!(b.to_str().unwrap(), "bar.exe"),
    ];
    let paths = which_all("bar").await.unwrap();
    assert_eq!(paths, expected_paths);

    cleanup_test_dir(&dir).await;
}
