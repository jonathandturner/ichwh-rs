//! Miscellaneous helper functions for Linux & MacOS

use {
    crate::{IchwhError, IchwhResult},
    async_std::{
        fs::DirEntry,
        path::{Path, PathBuf},
    },
    std::os::unix::fs::PermissionsExt,
};

/// Reads the `PATH` environment variable and splits it up into a list of directories.
///
/// # Errors
///
/// * `PATH` is not defined
pub fn split_path_env() -> IchwhResult<Vec<PathBuf>> {
    let path_var = std::env::var_os("PATH").ok_or(IchwhError::PathNotDefined)?;
    Ok(std::env::split_paths(&path_var).map(Into::into).collect())
}

/// Checks whether or not a file is executable.
///
/// # Errors
///
/// * An IO error occurs
pub async fn is_executable(file: &DirEntry) -> IchwhResult<bool> {
    let metadata = file.metadata().await?;

    let filetype = metadata.file_type();
    let permissions = metadata.permissions();

    // The file is executable if it is a directory or a symlink and the permissions are set for
    // owner, group, or other
    Ok((filetype.is_file() || filetype.is_symlink()) && (permissions.mode() & 0o111 != 0))
}

/// Checks whether or not a file is executable.
///
/// # Errors
///
/// * An IO error occurs
pub async fn is_executable_path<P: AsRef<Path>>(path: P) -> IchwhResult<Option<PathBuf>> {
    let metadata = path.as_ref().metadata().await?;

    let filetype = metadata.file_type();
    let permissions = metadata.permissions();

    // The file is executable if it is a directory or a symlink and the permissions are set for
    // owner, group, or other
    if (filetype.is_file() || filetype.is_symlink()) && (permissions.mode() & 0o111 != 0) {
        Ok(Some(path.as_ref().canonicalize().await?))
    } else {
        Ok(None)
    }
}
